package com.company;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

/* Voy a añadir un comentario para ver si se actualiza correctamente */
public class DiffFolder{
    private File folder1, folder2;

    public  DiffFolder() {
        folder1 = null;
        folder2 = null;
    }
    public static void main(String[] args) throws Exception {
	// write your code here
        File carpeta1 = new File("C:/Users/Alex/ArchivosJava/1");
        File carpeta2 = new File("C:/Users/Alex/ArchivosJava/2");
        DiffFolder comparacion = new DiffFolder();

        try {
            comparacion.setFolders(carpeta1, carpeta2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Ya que se pide que devolvamos un Iterator lo utilizaré para mostrar los resultados */
        Iterator<ResultadoComparacion> it = comparacion.comparar();
        while (it.hasNext()){
            ResultadoComparacion actual = it.next();
            System.out.println(actual.getNombre() + " " + actual.getValorComparacion());
        }

    }

    public void setFolders(File f1, File f2) throws Exception {
        if (f1.exists()==true && f2.exists()) {
            if(!f1.isDirectory()||!f2.isDirectory()) {
            /* Es necesario que existan en el sistma, en caso contrario se lanzaría una excepcion */
                throw new Exception("Los ficheros no son directorios");
            }else{
                folder1 = f1;
                folder2 = f2;
                /* Cumple las condiciones por lo tanto lo asigno */
            }
        } else {
            throw new Exception("Alguno de los directorios no existe en el sistema");
        }
    }

    public File getFolder1() {
        return folder1;
    }

    public File getFolder2() {
        return folder2;
    }

    public Iterator<ResultadoComparacion> comparar() {
        ArrayList<ResultadoComparacion> resultados = new ArrayList<>();

        for(File f1:folder1.listFiles()){
            for (File f2:folder2.listFiles()){
                /* Para que un archivo se considere igual según las instrucciones facilitadas
                debe cumplir tener el mismo nombre y el mismo tamaño
                 */
                if (f1.getName().equals(f2.getName())&&f1.length()==f2.length()){
                    /* Los archivos comparten nombres y tamaño */
                    if(f1.lastModified()<f2.lastModified()){
                        resultados.add(new ResultadoComparacion(f1.getName(),"MÁS NUEVO EN 1"));
                    }else if (f2.lastModified()<f1.lastModified()){
                        resultados.add(new ResultadoComparacion(f2.getName(),"MÁS NUEVO EN 2"));
                    }else {
                        resultados.add(new ResultadoComparacion(f1.getName(), "IGUALES"));
                    }
                }
            }
            /* Si no se ha nombrado el archivo hasta este momento es que no se ha encontrado en la segunda carpeta
            por lo que podemos afirmar que el archivo actual falta en la carpeta dos. Para saber si no se ha nombrado
            comprobamos el array de resultados hasta el momento y después
             */
            if(!resultados.contains(new ResultadoComparacion(f1.getName(),""))){
                /*  El segundo parámetro del constructor de Resultado da igual ya que
                    he sobrescrito el método equals para que
                    únicamente tenga en cuenta el nombre del archivo
                 */
                resultados.add(new ResultadoComparacion(f1.getName(),"FALTA_EN_2"));
            }
        }

        /* Compruebo los que quedan en la segunda carpeta */
        for (File f2:folder2.listFiles()){
            if(!resultados.contains(new ResultadoComparacion(f2.getName(),""))){
                /*  El segundo parámetro del constructor de Resultado da igual ya que
                    he sobrescrito el método equals para que
                    únicamente tenga en cuenta el nombre del archivo
                 */
                resultados.add(new ResultadoComparacion(f2.getName(),"FALTA_EN_1"));
            }
        }

        Iterator<ResultadoComparacion> it = resultados.iterator();
        return  it;
    }
}
