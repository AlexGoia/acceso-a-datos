package com.company;

public class ResultadoComparacion {
    private String nombre;
    private String valorComparacion;

    public ResultadoComparacion(String nombre, String valorComparacion) {
        this.nombre = nombre;
        this.valorComparacion = valorComparacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setValorComparacion(String valorComparacion) {
        this.valorComparacion = valorComparacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getValorComparacion() {
        return valorComparacion;
    }

    /* Para poder utilizar el .contains de los arraylist debo decir qué supone que un objeto sea igual a otro
    y en este caso utilizo el nombre
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultadoComparacion)) return false;

        ResultadoComparacion that = (ResultadoComparacion) o;

        return getNombre().equals(that.getNombre());
    }

    @Override
    public int hashCode() {
        return getNombre().hashCode();
    }
}
